# Git - Árbol de directorios 

# Uso de la terminal en git

Actividad para Gnubies donde por medio de git se despliegan en este repo varios archivos.

## Para empezar

Para realizar este repositorio primero se creo en gitlab, luego se creo una licencia GPL v3.0 para controlar nuestro pruducto y este cumpla con la filosofia del software libre.

### Prerequisitos

Se intalo este software de controlador de versiones.

* git

### Installation

Para la instalación de git se uso el siguiente comando que instala node.js junto a npm y git.

```
$ sudo apt-get install -y nodejs 

```

## Comandos subida

Para subir el repositorio se usaron los suguientes comandos en el siguiente orden.

```
$ git clone https://gitlab.com/Sergio.Acuna/git-arbol-de-directorios.git
$ mv Actividad_terminal git-arbol-de-directorios/
$ cd git-arbol-de-directorios/
$ ls
$ git init  
$ git remote add origin https://gitlab.com/Sergio.Acuna/git-arbol-de-directorios.git
$ git add . 
$ git commit -m "Initial commit"
$ git push -u origin main
$ ls 
$ git add .
$ git commit -m "Second commit"
$ git push -u origin main
```


### Branches

* Main


## Documentación adicional

* Este template fue tomado de https://www.readme-templates.com/

## Creador del repositorio 

* Sergio Stiven Acuña Valderrama, usuario en gitlab: Sergio.Acuna 
